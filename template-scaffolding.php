<?php
/**
 * Template Name: Scaffolding
 *
 * Template Post Type: page, scaffolding, ptig_test_scaffolding
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Test Theme
 */

get_header(); ?>

	<div class="primary content-area">
		<main id="main" class="site-main">
			<?php do_action( 'ptig_test_scaffolding_content' ); ?>
		</main><!-- #main -->
	</div><!-- .primary -->

<?php get_footer(); ?>
