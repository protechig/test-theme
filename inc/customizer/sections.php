<?php
/**
 * Customizer sections.
 *
 * @package Test Theme
 */

/**
 * Register the section sections.
 *
 * @param object $wp_customize Instance of WP_Customize_Class.
 */
function ptig_test_customize_sections( $wp_customize ) {

	// Register additional scripts section.
	$wp_customize->add_section(
		'ptig_test_additional_scripts_section',
		array(
			'title'    => esc_html__( 'Additional Scripts', 'test-site' ),
			'priority' => 10,
			'panel'    => 'site-options',
		)
	);

	// Register a social links section.
	$wp_customize->add_section(
		'ptig_test_social_links_section',
		array(
			'title'       => esc_html__( 'Social Media', 'test-site' ),
			'description' => esc_html__( 'Links here power the display_social_network_links() template tag.', 'test-site' ),
			'priority'    => 90,
			'panel'       => 'site-options',
		)
	);

	// Register a header section.
	$wp_customize->add_section(
		'ptig_test_header_section',
		array(
			'title'    => esc_html__( 'Header Customizations', 'test-site' ),
			'priority' => 90,
			'panel'    => 'site-options',
		)
	);

	// Register a footer section.
	$wp_customize->add_section(
		'ptig_test_footer_section',
		array(
			'title'    => esc_html__( 'Footer Customizations', 'test-site' ),
			'priority' => 90,
			'panel'    => 'site-options',
		)
	);
}
add_action( 'customize_register', 'ptig_test_customize_sections' );
