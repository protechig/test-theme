<?php
/**
 * The template used for displaying X in the scaffolding library.
 *
 * @package Test Theme
 */

?>

<section class="section-scaffolding">
	<h2 class="scaffolding-heading"><?php esc_html_e( 'X', 'test-site' ); ?></h2>
</section>
